var map;

var marker_list = [];



function getPointsListJson() {
    var json = "[";

    for (var i=0; i<marker_list.length; i++) {
        var marker_object = marker_list[i];

        json += marker_object.toJsonString();

        if(i<(marker_list.length-1)){
            json +=",";
        }

    }

    json += "]";
    return json;
}


function initMap(points_list) {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: {lat: 50.401699, lng: 30.2525047},
        zoom: 8
    });

    var address_panel = document.getElementById('address_view');
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(
        address_panel);

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }


        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });

    if(points_list.length>0){
        parcePoints(points_list);
    }

}

function parcePoints(point_list){
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i=0; i<point_list.length; i++) {
        var point = point_list[i];
        var latLng = {lat: point.LATITUDE, lng: point.LONGITUDE};
        createMarkerType(latLng,point.POINT_TYPE,latlngbounds);
    }

    map.setCenter(latlngbounds.getCenter(), map.fitBounds(latlngbounds));

}


function createMarkerType(latLng,marker_type,latlngbounds){

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable:true,
        title:""
    });
    var marker_object = new MarkerObject(marker);
    marker_object.setMarkerType(MarkerObject.MARKER_POINT_TYPE);
    marker.addListener('click', function() {
        showMarkerInfo(marker_object);

    });

    marker_list.push(marker_object);
    latlngbounds.extend(latLng);
}

function showMarkerInfo(marker_object){
    document.getElementById('name_place').innerHTML = '<strong>' + marker_object.getNearestCity() + '</strong>';
    document.getElementById('adminName1').value = marker_object.getStateProvince();
    document.getElementById('countryCode').value = marker_object.getCountryCode();
    document.getElementById('distance').value = marker_object.getNearestCityDistance();
    document.getElementById('lat').value = marker_object.getMarker().getPosition().lat();
    document.getElementById('lng').value = marker_object.getMarker().getPosition().lng();
    document.getElementById('name').value = marker_object.getNearestCity();
    document.getElementById('typePosition').value = marker_object.getTypeOfSurface();
}


