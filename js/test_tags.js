/**
 * Created by roman on 11/25/16.
 */

function callBtn() {

    var t_input = document.getElementById("pac-input");
    showTags(t_input.value);

}

function showTags(tagString){
    var tagListArray;
    tagListArray = getHashTags(tagString);

    tagListArray.forEach(function(item, i, arr) {
        console.log( i + ": " + item + " (массив:" + arr + ")" );
    });

    /*for (var w in tagListArray) {
        console.log(w);
    }
    */

}

function getHashTags(tagString)
{
    var tagListArray = [];
    var regexp = new RegExp('#([^\\s]*)', 'g');
    var tmplist = tagString.match(regexp);
    for (var w in tmplist) {
        var hashSub = tmplist[ w ].split('#');
        for (var x in hashSub) {
            if (hashSub[x] != "")
            {
                if (hashSub[x].substr(hashSub[x].length - 1) == ":")
                {
                    hashSub[x] = hashSub[x].slice(0, -1);
                }
                if (hashSub[x] != "") {
                    tagListArray.push(hashSub[x]);
                }
            }
        }
    }
    return tagListArray;
}
