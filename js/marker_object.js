//TODO set validator on simultanuously start end transit
function MarkerObject(marker,g_map,callback) {
    this._start = false;
    this._end = false;
    this._transit = false;
    this.marker_= marker;
    this.currentPositionName_ = "";
    this.g_map_ = g_map;
    this.updateLatLngInfo(marker.position);
    this.updateFunction_ = callback;
    this.setCountryCode("IJ");
}


MarkerObject.MARKER_TRANSIT_TYPE_ICON = 'icons/markers/transit_marker.png';
MarkerObject.MARKER_POINT_TYPE_ICON = 'icons/markers/point_marker.png';
MarkerObject.MARKER_WATER_TYPE_ICON = 'icons/markers/water_marker.png';
MarkerObject.MARKER_START_TYPE_ICON = 'icons/markers/start_marker.png';
MarkerObject.MARKER_FINISH_TYPE_ICON = 'icons/markers/finish_marker.png';
// MarkerObject.MARKER_UNDEFINED_TYPE_ICON = 'icons/markers/temp_marker.png';

// MarkerObject.MARKER_START_TYPE = 0;
// MarkerObject.MARKER_FINISH_TYPE = 1;
MarkerObject.MARKER_POINT_TYPE = 2;
// MarkerObject.MARKER_TRANSIT_TYPE = 3;
MarkerObject.MARKER_WATER_TYPE = 4;

MarkerObject.prototype.isStart = function() {
  return this._start;
};

MarkerObject.prototype.setStart = function(val) {
    this._start = val;
};

MarkerObject.prototype.isEnd = function() {
    return this._end;
};

MarkerObject.prototype.setEnd = function(val) {
    this._end = val;
};

MarkerObject.prototype.isTransit = function() {
    return this._transit;
};

MarkerObject.prototype.setTransit = function(val) {
    this._transit = val;
};

MarkerObject.prototype.setMarker=function (marker) {
    this.marker_=marker;
    this.updateLatLngInfo(marker.position);
};

MarkerObject.prototype.getMarker=function () {
    return this.marker_;
};

MarkerObject.prototype.setMarkerType = function() {
    if( this.getTypeOfSurface() == 1 ) {
        this.marker_type_ = MarkerObject.MARKER_WATER_TYPE;
    } else {
        this.marker_type_ = MarkerObject.MARKER_POINT_TYPE;
    }
};

MarkerObject.prototype.getMarkerType=function () {
    return this.marker_type_;
};

MarkerObject.prototype.changeMarkerTypeAndIcon = function(type) {
    if ( type ) {
        this.marker_type_ = type;
    }
    this.setMarkerIcon();
};

MarkerObject.prototype.setMarkerIcon=function () {
    this.marker_.setIcon(this.getMarkerIcon(this.getMarkerType()));
};

MarkerObject.prototype.getMarkerIcon=function (marker_type) {
    // if(marker_type==MarkerObject.MARKER_TRANSIT_TYPE){
    //     return MarkerObject.MARKER_TRANSIT_TYPE_ICON;
    // }
    if ( this.isStart() ) {
        return MarkerObject.MARKER_START_TYPE_ICON;
    }
    if ( this.isEnd() ) {
        // alert(MarkerObject.MARKER_FINISH_TYPE_ICON);
        return MarkerObject.MARKER_FINISH_TYPE_ICON;
    }
    if ( marker_type == MarkerObject.MARKER_POINT_TYPE ) {
        return MarkerObject.MARKER_POINT_TYPE_ICON;
    }
    if ( marker_type == MarkerObject.MARKER_WATER_TYPE ) {
        return MarkerObject.MARKER_WATER_TYPE_ICON;

    }
};
MarkerObject.prototype.setCountryCode=function (country_code) {
    this.country_code_ = country_code;
};

MarkerObject.prototype.getCountryCode=function () {
    return this.country_code_;
};

MarkerObject.prototype.setNearestCity=function (city_name) {
    this.nearest_city_ = city_name;
};

MarkerObject.prototype.getNearestCity=function () {
    return this.nearest_city_;
};

MarkerObject.prototype.getNearestCityOrWater=function () {
    if (this.getTypeOfSurface()==1){
        return this.getWaterName();
    }
    return this.nearest_city_;
};

MarkerObject.prototype.setNearestCityDistance=function (distance) {
    this.nearest_city_distance_ = distance;
};

MarkerObject.prototype.getNearestCityDistance=function () {
    return this.nearest_city_distance_;
};

MarkerObject.prototype.setTypeOfSurface=function (type_of_surface) {
    this.type_of_surface_ = type_of_surface;
};

MarkerObject.prototype.getTypeOfSurface=function () {
    return this.type_of_surface_;
};

MarkerObject.prototype.setWaterName=function (water_name) {
    this.water_name_ = water_name;
};

MarkerObject.prototype.getWaterName=function () {
    return this.water_name_;
};

MarkerObject.prototype.setStateProvince=function (state_province) {
    this.state_province_ = state_province;
};

MarkerObject.prototype.getStateProvince=function () {
    return this.state_province_;
};

MarkerObject.prototype.setStartPolyLine=function (start_polyline) {
    this.start_polyline_ = start_polyline;
};

MarkerObject.prototype.getStartPolyLine=function () {
    return this.start_polyline_;
};

MarkerObject.prototype.setEndPolyLine=function (end_polyline) {
    this.end_polyline_ = end_polyline;
};

MarkerObject.prototype.getEndPolyLine=function () {
    return this.end_polyline_;
};

MarkerObject.prototype.setAddress=function (address_info) {
    this.address_ = address_info;
};

MarkerObject.prototype.getAddress=function () {
    return this.address_;
};

MarkerObject.prototype.getCurrentPositionName=function () {
    return this.currentPositionName_;
};

MarkerObject.prototype.setCurrentPositionName=function (value) {
    this.currentPositionName_ = value;
};

MarkerObject.prototype.toJsonString=function () {
    var json = "{";

        json += "\"POINT_TYPE\":"+this.marker_type_+",";
        json += "\"LONGITUDE\":"+this.marker_.getPosition().lng()+",";
        json += "\"LATITUDE\":"+this.marker_.getPosition().lat()+",";
        json += "\"COUNTRY_CODE\":\""+this.country_code_+"\",";
        json += "\"CITY_NAME\":\""+this.nearest_city_+"\",";
        json += "\"CITY_DISTANCE\":"+this.nearest_city_distance_+",";
        json += "\"LAND_TYPE\":"+this.type_of_surface_+",";
        json += "\"WATER_NAME\":\""+""+"\",";
        json += "\"STATE_PROVINCE\":\""+this.state_province_+"\"";

    json += "}";
    return json;
};

MarkerObject.prototype.updateWaterInfo=function () {
    var $this = this;
    var service = new google.maps.places.PlacesService(map);
    var latLng = this.marker_.getPosition();


    service.nearbySearch({
        location: {lat: latLng.lat(), lng: latLng.lng() },
        rankBy: google.maps.places.RankBy.DISTANCE,
        language:'en',
        types:['natural_feature']
    }, nearbyCallback);

    function nearbyCallback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            console.log("OK:" + results.length);
            for (var i = 0; i < 1; i++) {
                $this.setWaterName(results[i].name);
                $this.updateLocalityInfo();
            }
        } else {
            $this.updateLocalityInfo();
        }
    }
};

MarkerObject.prototype.updateLocalityInfo=function () {

    var $this = this;
    var service = new google.maps.places.PlacesService(map);
    var latLng = this.marker_.getPosition();

    // service.nearbySearch({
    //     location: {lat: latLng.lat(), lng: latLng.lng() },
    //     radius: '2000',
    //     types: ['locality']
    // }, nearbyCallback);




    // service.radarSearch({
    //      location: {lat: latLng.lat(), lng: latLng.lng() },
    //      radius:50000,
    //      types:['natural_feature']
    //  }, nearbyCallback);

    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng.lat() + "," + latLng.lng() + "";
    $.get(url).success(function(data) {
        var locInfo = data.results[0];
        var undefLocality = false;
        var county, city;
        $.each(locInfo, function(conponents,value) {

            if (conponents == "address_components") {
                for (var i = 0; i < value.length; i++) {
                    for (type in value[i]) {
                        if (type == "types") {
                            var types = value[i][type];
                            if (types[0] =="sublocality_level_1") {
                                county = value[i].long_name;
                                alert ("county: " + value[i].long_name);
                            }
                            if (types[0] =="locality") {
                                city = value[i].long_name;
                                $this.setCurrentPositionName(city);
                                undefLocality = true;
                            }



                            if (types[0] =="administrative_area_level_1") {
                                // city = value[i].long_name;
                                // alert ("administrative_area_level_1: " + value[i].long_name);
                            }
                            // if (types[0] =="postal_code") {
                            //     // city = value[i].long_name;
                            //     alert ("postal_code: " + value[i].long_name);
                            // }

                            // alert(types[0]);
                        }

                    }

                }

            }

        });

        if ( undefLocality == false ) {
            service.nearbySearch({
                location: {lat: latLng.lat(), lng: latLng.lng() },
                rankBy: google.maps.places.RankBy.DISTANCE,
                types:['locality']
            }, nearbyCallback);
        }
    });
    function nearbyCallback(results, status) {

        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < 1; i++) {
                // console.log(results[i]);
                // alert(results[i]);
                // alert(results[i].name);
                // this.setCurrentPositionName(undefined);
                $this.setNearestCity(results[i].name);
                $this.calculateNearestDistance(results[i].geometry.location);
                $this.getDetailsAddress(results[i].place_id);

            }
            // $this.callUpdateFunction($this);
        } else {

        }
    }

};

MarkerObject.prototype.calculateNearestDistance=function (latLng) {
    this.setNearestCityDistance((google.maps.geometry.spherical.computeDistanceBetween(this.marker_.getPosition(), latLng) / 1000).toFixed(2));
    // alert(this.getNearestCity());
};

MarkerObject.prototype.getDetailsAddress=function(place_id){
    var $this = this;
    var service = new google.maps.places.PlacesService(map);
    service.getDetails({
        placeId: place_id,
        language:'en'
    }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            console.log(place);
            $this.setAddress(place.formatted_address);

            var address_list = place.address_components;

            for (var i = 0; i < address_list.length; i++) {
                var address_types = address_list[i];
                if(address_types.types[0]=='administrative_area_level_1'){
                    $this.setStateProvince(address_types.long_name);
                }
                if(address_types.types[0]=='country'){
                    $this.setCountryCode(address_types.short_name);
                }
                if(address_types.types[0]=='locality'){
                    $this.setNearestCity(address_types.long_name);
                }
            }

        }
    });
};


MarkerObject.prototype.updateLatLngInfo=function (latLng) {


    // this.setWaterName('undefined');
    // this.setCountryCode('');
    // this.setStateProvince('undefined');
    // this.setNearestCityDistance(0);
    // this.setNearestCity('undefined');
    // this.setAddress('undefined');

    var $this = this;

    var myData = [];

    var img = new Image();
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    img.crossOrigin = 'http://maps.googleapis.com/crossdomain.xml';
    img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + latLng.lat() + "," + latLng.lng() + "&size=1x1&maptype=roadmap&sensor=false&zoom=12&key=AIzaSyAm_E3BzrUTgihqynjvSbfRx1ViYYz-CfM&&visual_refresh=true&style=element:labels|visibility:off&style=feature:water|color:0x00FF00&style=feature:transit|visibility:off&style=feature:poi|visibility:off&style=feature:road|visibility:off&style=feature:administrative|visibility:off";

    img.onload = function () {
        canvas.width = img.width;
        canvas.height = img.height;
        context.drawImage(img, 0, 0);
        myData = context.getImageData(0, 0, img.width, img.height).data;

        if ( $this.checkIsWater(myData) == true ) {
            $this.setTypeOfSurface(1);
        } else {
            $this.setTypeOfSurface(0);
        }
        context.clearRect(0, 0, canvas.width, canvas.height);
        $this.setMarkerType();
        $this.setMarkerIcon();
        $this.updateLocalityInfo();
        $this.updateWaterInfo();
    };


};

MarkerObject.prototype.checkIsWater = function (data) {
    var water_color_bytes = [0, 255, 0],
        our_color_bytes = [data[0], data[1], data[2]];

    for (var i = 0; i < water_color_bytes.length; i++) {

        if (water_color_bytes[i] != our_color_bytes[i]) {

            // this.setTypeOfSurface(0);
            // this.setWaterName('undefined');
            // this.updateLocalityInfo();

            return false;
        }
    }

    // this.setTypeOfSurface(1);
    // this.updateWaterInfo();
    return true;
};

MarkerObject.prototype.callUpdateFunction=function (marker_obj) {

    this.updateFunction_(marker_obj);
};