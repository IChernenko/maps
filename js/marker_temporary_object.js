function MarkerTemporaryObject(marker){
    this.marker_=marker;
}

MarkerTemporaryObject.MARKER_TEMPORARY_TYPE_ICON = 'icons/markers/start_marker.png';

MarkerTemporaryObject.MARKER_TEMPORARY_TYPE = 4;

MarkerTemporaryObject.prototype.setMarker=function (marker) {
    this.marker_=marker;
    this.updateLatLngInfo(marker.position);
};

MarkerTemporaryObject.prototype.getMarker=function () {
    return this.marker_;
};

MarkerTemporaryObject.prototype.setMarkerType=function (marker_type) {

    if(marker_type!=this.marker_type_) {
        this.marker_type_ = marker_type;
        this.marker_.setIcon(this.getMarkerIcon(marker_type));
    }
};

MarkerTemporaryObject.prototype.getMarkerIcon=function (marker_type) {
    if(marker_type==MarkerTemporaryObject.MARKER_TEMPORARY_TYPE){
        return MarkerTemporaryObject.MARKER_TEMPORARY_TYPE_ICON;
    }
};

MarkerTemporaryObject.prototype.getMarkerType=function () {
    return this.marker_type_;
};

MarkerTemporaryObject.prototype.setStartPolyLine=function (start_polyline) {
    this.start_polyline_ = start_polyline;
};

MarkerTemporaryObject.prototype.getStartPolyLine=function () {
    return this.start_polyline_;
};

MarkerTemporaryObject.prototype.setEndPolyLine=function (end_polyline) {
    this.end_polyline_ = end_polyline;
};

MarkerTemporaryObject.prototype.getEndPolyLine=function () {
    return this.end_polyline_;
};

MarkerTemporaryObject.prototype.setMainMarker=function (main_marker) {
    this.main_marker_ = main_marker;
};

MarkerTemporaryObject.prototype.getMainMarker=function () {
    return this.main_marker_;
};