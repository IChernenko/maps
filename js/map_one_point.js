/**
 * Created by roman on 9/28/16.
 */
var map;
var marker_current;
var contextMenu;


function getPointsListJson() {
    var json = "[";

    if(marker_current!=null) {
        json += marker_current.toJsonString();
    }



    json += "]";
    return json;
}


function initMap(points_list) {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: {lat: 50.401699, lng: 30.2525047},
        zoom: 8
    });

    contextMenu=initiContextMenu();

    google.maps.event.addListener(map, 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginItem').style.display='block';
        document.getElementById('directionsRemoveItem').style.display='none';
        contextMenu.show(mouseEvent.latLng,map);
    });


    google.maps.event.addListener(contextMenu, 'menu_item_selected', function (latLng, eventName, context) {
        switch(eventName){
            case 'directions_origin_click':

                if (marker_current!=null){
                    marker_current.getMarker().setMap(null);
                }
                marker_current = createMarker(latLng,"Test");

                break;


            case 'directions_point_remove':

                if (marker_current!=null){
                    marker_current.getMarker().setMap(null);
                }
                marker_current = null;

                break;

            case 'zoom_in_click':
                map.setZoom(map.getZoom()+1);
                break;
            case 'zoom_out_click':
                map.setZoom(map.getZoom()-1);
                break;
            case 'center_map_click':
                map.panTo(latLng);
                break;
        }

    });

    var address_panel = document.getElementById('address_view');
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(
        address_panel);

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        if (places.length == 0) {
            return;
        }



        // For each place, get the icon, name and location.
        //var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchormarker_current: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };


            if (marker_current!=null){
                marker_current.getMarker().setMap(null);
            }

            marker_current = createMarker(place.geometry.location,"Test");
            //centerPoints(marker_list);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

            document.getElementById('pac-input').value ="";

        });
        map.fitBounds(bounds);
    });

    if(points_list.length>0){
        parcePoints(points_list);
        centerPoints();
    }

}

function centerPoints(){
    var latlngbounds = new google.maps.LatLngBounds();
        var point = marker_current;
        latlngbounds.extend(point.getMarker().position);

    map.setCenter(latlngbounds.getCenter(), map.fitBounds(latlngbounds));

}

function removePointFromArray(marker_object){
    for (var i=0; i<marker_list.length; i++) {
        if(marker_list[i]==marker_object){
            marker_list.splice(i,1);
            break;
        }
    }
}

function addPointToArray(marker_object,next_marker){
    if(marker_object.getMarkerType()!=MarkerObject.MARKER_TRANSIT_TYPE){
        marker_list.push(marker_object);
    }else{
        for (var i=0; i<marker_list.length; i++) {
            if(marker_list[i]==next_marker){
                marker_list.splice(i,0,marker_object);
                break;
            }
        }
    }
}

function parcePoints(point_list){
    for (var i=0; i<1; i++) {
        var point = point_list[i];
        var latLng = {lat: point.LATITUDE, lng: point.LONGITUDE};
        createMarkerType(latLng,MarkerObject.MARKER_POINT_TYPE);
    }


}


function createMarkerType(latLng,marker_type){

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable:true,
        title:""
    });
    var marker_object = new MarkerObject(marker);
    marker_object.setMarkerType(marker_type);
    marker.addListener('click', function() {
        showMarkerInfo(marker_object);

    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        var latLng = event.latLng;
        marker_object.updateLatLngInfo(latLng);
        console.log(latLng);
    });



    google.maps.event.addListener(marker_object.getMarker(), 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsRemoveItem').style.display = 'block';

        contextMenu.show(mouseEvent.latLng,marker_object);
    });

    marker_current = marker_object;


}


function initiContextMenu() {
    var contextMenuOptions={};
    contextMenuOptions.classNames={menu:'context_menu', menuSeparator:'context_menu_separator'};

    var menuItems=[];
    menuItems.push({className:'context_menu_item', eventName:'directions_origin_click', id:'directionsOriginItem', label:'Добавить точку'});
    menuItems.push({className:'context_menu_item', eventName:'directions_point_remove', id:'directionsRemoveItem', label:'Удалить точку'});
    menuItems.push({});
    menuItems.push({className:'context_menu_item', eventName:'zoom_in_click', label:'Zoom in'});
    menuItems.push({className:'context_menu_item', eventName:'zoom_out_click', label:'Zoom out'});
    menuItems.push({});
    menuItems.push({className:'context_menu_item', eventName:'center_map_click', label:'Center map here'});
    contextMenuOptions.menuItems=menuItems;

    var contextMenuOriginal=new ContextMenu(map, contextMenuOptions);

    return contextMenuOriginal;
}


function createMarker(markerLatlng,titleMarker) {

    var marker = new google.maps.Marker({
        position: markerLatlng,
        map: map,
        draggable:true,
        title:titleMarker
    });

    var marker_object = new MarkerObject(marker);



    if(titleMarker=="Transit") {
        marker_object.setMarkerType(MarkerObject.MARKER_TRANSIT_TYPE);

    }else{
        marker_object.setMarkerType(MarkerObject.MARKER_POINT_TYPE);

    }
    marker.addListener('click', function() {
        showMarkerInfo(marker_object);

    });



    google.maps.event.addListener(marker, 'dragend', function(event) {
        var latLng = event.latLng;
        marker_object.updateLatLngInfo(latLng);
        console.log(latLng);
    });

    google.maps.event.addListener(marker, 'drag', function(event) {
        var latLng = event.latLng;
        var poly_line_start = marker_object.getStartPolyLine();
        if(poly_line_start!=null) {
            var paths = poly_line_start.polyline.getPath().getArray();
            var path = [paths[0], latLng];
            poly_line_start.polyline.setPath(path);
        }

        var poly_line_end = marker_object.getEndPolyLine();

        if(poly_line_end!=null) {
            var paths = poly_line_end.polyline.getPath().getArray();
            var path = [latLng,paths[1]];
            poly_line_end.polyline.setPath(path);
        }

    } );

    google.maps.event.addListener(marker_object.getMarker(), 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsRemoveItem').style.display = 'block';
        contextMenu.show(mouseEvent.latLng,marker_object);
    });


    return marker_object;
}

function showMarkerInfo(marker_object){
    document.getElementById('name_place').innerHTML = '<strong>' + marker_object.getNearestCity() + '</strong>';
    document.getElementById('adminName1').value = marker_object.getStateProvince();
    document.getElementById('countryCode').value = marker_object.getCountryCode();
    document.getElementById('distance').value = marker_object.getNearestCityDistance();
    document.getElementById('lat').value = marker_object.getMarker().getPosition().lat();
    document.getElementById('lng').value = marker_object.getMarker().getPosition().lng();
    document.getElementById('name').value = marker_object.getNearestCity();
    document.getElementById('typePosition').value = marker_object.getTypeOfSurface();
    document.getElementById('address_info').value = marker_object.getAddress();
}


