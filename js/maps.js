var country_flags_url = 'http://development.apps-check.com/design/images/icons/flags/flags_NEW/';


var map;
var marker_last;
var marker_first;
var marker_list = [];

var move_marker_polyline;
var current_polyline;

var last_search_position;

var current_marker_object;
var infowindow;


function getPointsListJson() {
    var json = "[";

    for (var i=0; i<marker_list.length; i++) {
        var marker_object = marker_list[i];

        json += marker_object.toJsonString();

        if(i<(marker_list.length-1)){
            json +=",";
        }

    }

    json += "]";
    return json;
}

function createCurrentMarker() {

    if(last_search_position){
        var marker_temp = createMarker(last_search_position,"Test");
        // alert(marker_temp.getMarkerType());
        drawPolyline(marker_temp);
        addPointToArray(marker_temp,null);
    }

}

function closeInfoWindow(){
    if(infowindow){
        infowindow.close();
    }
}



function initMap(points_list) {

    map = new google.maps.Map(document.getElementById('map_canvas'), {
        center: {lat: 50.401699, lng: 30.2525047},
        zoom: 8,
        zoomControl: false,
        mapTypeControl: false,
    });

    //contextMenu=initiContextMenu();
    //contextMenuOptions.classNames={menu:'context_menu', menuSeparator:'context_menu_separator'};
    //var contextMenu=new ContextMenuMarkers(map, contextMenuOptions);


    infowindow = new google.maps.InfoWindow({
        content: '',
        maxWidth: 350
    });

    google.maps.event.addListener(infowindow, 'domready', function() {

        // Reference to the DIV that wraps the bottom of infowindow
        var iwOuter = $('.gm-style-iw');

        /* Since this div is in a position prior to .gm-div style-iw.
         * We use jQuery and create a iwBackground variable,
         * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
         */
        var iwBackground = iwOuter.prev();

        // Removes background shadow DIV
        iwBackground.children(':nth-child(2)').css({'display' : 'none'});

        // Removes white background DIV
        iwBackground.children(':nth-child(4)').css({'display' : 'none'});

        // Moves the infowindow 115px to the right.
        iwOuter.parent().parent().css({left: '40px'});

        // Moves the shadow of the arrow 76px to the left margin.
        iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

        // Moves the arrow 76px to the left margin.
        iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

        // Changes the desired tail shadow color.
        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 100, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

        // Reference to the div that groups the close button elements.
        var iwCloseBtn = iwOuter.next();

        // Apply the desired effect to the close button
        iwCloseBtn.css({opacity: '1', right: '20px', top: '20px'});

        // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
        if($('.iw-content').height() < 140){
            $('.iw-bottom-gradient').css({display: 'none'});
        }

        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
        });
    });



    google.maps.event.addListener(map, 'click', function(mouseEvent) {
        last_search_position = mouseEvent.latLng;
        createCurrentMarker();
        closeInfoWindow();
    });

    google.maps.event.addListener(map, 'rightclick', function(mouseEvent) {
        closeInfoWindow();
    });







    var search_panel = document.getElementById('top_menu_map');
    var zoom_panel = document.getElementById('zoom_menu_map');

    var button_create_marker = document.getElementById('button-marker');

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(search_panel);
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(zoom_panel);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        clearTemporaryMarker();
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        clearTemporaryMarker();
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }


        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            /*
             markers.push(new google.maps.Marker({
             map: map,
             icon: icon,
             title: place.name,
             position: place.geometry.location
             }));
             */
            last_search_position = place.geometry.location;
            //var marker_temp = createMarker(place.geometry.location,"Test");
            //drawPolyline(marker_temp);
            //addPointToArray(marker_temp,null);


            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }

            //centerPoints(marker_list);
            //marker_list

            //document.getElementById('pac-input').value ="";

        });
        map.fitBounds(bounds);
    });

    if(points_list.length>0){
        parcePoints(points_list);
    }

}

function zoomMapIncrement(){
    map.setZoom(map.getZoom()+1);
}

function zoomMapDecrement(){
    map.setZoom(map.getZoom()-1);
}

function clearTemporaryMarker(){
    if(move_marker_polyline!=null){
        if(move_marker_polyline.getStartPolyLine()!=null){
            move_marker_polyline.getStartPolyLine().polyline.setMap(null);
            move_marker_polyline.setStartPolyLine(null);
        }

        if(move_marker_polyline.getEndPolyLine()!=null){
            move_marker_polyline.getEndPolyLine().polyline.setMap(null);
            move_marker_polyline.setEndPolyLine(null);
        }

        move_marker_polyline.getMarker().setMap(null);
        move_marker_polyline = null;
    }
}

function centerPoints(point_list){
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i=0; i<point_list.length; i++) {
        var point = point_list[i];
        latlngbounds.extend(point.getMarker().position);
    }

    map.setCenter(latlngbounds.getCenter(), map.fitBounds(latlngbounds));

}

function removePointFromArray(marker_object){
    for (var i=0; i<marker_list.length; i++) {
        if(marker_list[i]==marker_object){
            marker_list.splice(i,1);
            break;
        }
    }
}

function addPointToArray(marker_object,next_marker){
    if(marker_object.getMarkerType()!=MarkerObject.MARKER_TRANSIT_TYPE){
        marker_list.push(marker_object);
    }else{
        for (var i=0; i<marker_list.length; i++) {
            if(marker_list[i]==next_marker){
                marker_list.splice(i,0,marker_object);
                break;
            }
        }
    }
}

function parcePoints(point_list){
    for (var i=0; i<point_list.length; i++) {
        var point = point_list[i];
        var latLng = {lat: point.LATITUDE, lng: point.LONGITUDE};
        createMarkerType(latLng,point.POINT_TYPE);
    }

    var prev_marker = marker_list[0];

    for (var i=1; i<marker_list.length; i++) {
        var next_marker = marker_list[i];
        createPolyLineMarkers(prev_marker,next_marker);
        prev_marker = next_marker;
    }
}

function createPolyLineMarkers(prev_marker, next_marker){

    if((prev_marker.getMarkerType()!=MarkerObject.MARKER_TRANSIT_TYPE)&&(next_marker.getMarkerType()!=MarkerObject.MARKER_TRANSIT_TYPE)){
        var polyline = createPolyline(prev_marker,next_marker);

        google.maps.event.addListener(polyline.polyline, 'rightclick', function(mouseEvent){
            document.getElementById('directionsOriginalTransitItem').style.display='none';
            document.getElementById('directionsTransitOriginalItem').style.display='none';
            document.getElementById('directionsTransitItem').style.display='block';
            document.getElementById('directionsOriginItem').style.display='none';
            document.getElementById('directionsRemoveItem').style.display='none';
            document.getElementById('directionsRemoveTransitItem').style.display='none';
            document.getElementById('directionsTransitPointItem').style.display='none';
            document.getElementById('directionsOriginalPointItem').style.display='none';
            document.getElementById('directionsCancelItem').style.display='none';
            //contextMenu.show(mouseEvent.latLng,polyline);
        });

        google.maps.event.addListener(polyline.polyline, 'mousemove', function(mouseEvent){
            moveMarkerPolyLine(mouseEvent.latLng,polyline);
        });

    }else{
        var transit_polyline = createPolyline(prev_marker,next_marker);
        google.maps.event.addListener(transit_polyline.polyline, 'rightclick', function(mouseEvent){
            document.getElementById('directionsOriginalTransitItem').style.display='none';
            document.getElementById('directionsTransitOriginalItem').style.display='none';
            document.getElementById('directionsTransitItem').style.display='block';
            document.getElementById('directionsOriginItem').style.display='none';
            document.getElementById('directionsRemoveItem').style.display='none';
            document.getElementById('directionsRemoveTransitItem').style.display='none';
            document.getElementById('directionsTransitPointItem').style.display='none';
            document.getElementById('directionsOriginalPointItem').style.display='none';
            document.getElementById('directionsCancelItem').style.display='none';
            //contextMenu.show(mouseEvent.latLng,transit_polyline);
        });

        google.maps.event.addListener(transit_polyline.polyline, 'mousemove', function(mouseEvent){
            moveMarkerPolyLine(mouseEvent.latLng,transit_polyline);
        });
    }

}

function createMarkerType(latLng,marker_type) {

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable:true,
        title:""
    });

    var marker_object = new MarkerObject(marker,map,updateMarkerInfo);
    marker_object.setMarkerType(marker_type);
    marker.addListener('click', function() {
        showMarkerInfo(marker_object);
    });
    marker.addListener('rightclick', function() {
        showM(marker_object);
    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        closeInfoWindow();
        var latLng = event.latLng;
        marker_object.updateLatLngInfo(latLng);
        console.log(latLng);
    });


    google.maps.event.addListener(marker, 'drag', function(event) {

        var latLng = event.latLng;
        var poly_line_start = marker_object.getStartPolyLine();
        if(poly_line_start!=null) {
            var paths = poly_line_start.polyline.getPath().getArray();
            var path = [paths[0], latLng];
            poly_line_start.polyline.setPath(path);
        }

        var poly_line_end = marker_object.getEndPolyLine();

        if(poly_line_end!=null) {
            var paths = poly_line_end.polyline.getPath().getArray();
            var path = [latLng,paths[1]];
            poly_line_end.polyline.setPath(path);
        }

    } );

    google.maps.event.addListener(marker_object.getMarker(), 'rightclick', function(mouseEvent){
        document.getElementById('directionsTransitItem').style.display='none';
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsTransitPointItem').style.display='none';
        document.getElementById('directionsOriginalPointItem').style.display='none';
        document.getElementById('directionsCancelItem').style.display='none';
        if(marker_object.getMarkerType() == MarkerObject.MARKER_TRANSIT_TYPE){
            document.getElementById('directionsOriginalTransitItem').style.display='block';
            document.getElementById('directionsTransitOriginalItem').style.display='none';
            document.getElementById('directionsRemoveItem').style.display='none';
            document.getElementById('directionsRemoveTransitItem').style.display='block';
        }else {
            document.getElementById('directionsOriginalTransitItem').style.display='none';
            document.getElementById('directionsTransitOriginalItem').style.display='block';
            document.getElementById('directionsRemoveItem').style.display = 'block';
            document.getElementById('directionsRemoveTransitItem').style.display = 'none';
        }

        if((marker_object.getMarkerType() == MarkerObject.MARKER_START_TYPE)||
            (marker_object.getMarkerType() == MarkerObject.MARKER_FINISH_TYPE)){
            document.getElementById('directionsOriginalTransitItem').style.display='none';
            document.getElementById('directionsTransitOriginalItem').style.display='none';
        }

        contextMenu.show(mouseEvent.latLng,marker_object);
    });


    if(marker_type==MarkerObject.MARKER_START_TYPE){
        marker_first = marker_object;
    }else
    if(marker_type==MarkerObject.MARKER_FINISH_TYPE){
        marker_last = marker_object;
    }

    marker_list.push(marker_object);
}



function drawPolyline(marker_new) {
    // alert(marker_new.getMarkerType());
    if(marker_first!=null){

        var markerPrevious = {};


        if(marker_last==null){
            markerPrevious = marker_first;
        }else{
            markerPrevious = marker_last;
        }

        var markerNext = marker_new;
        markerNext.setMarkerType(MarkerObject.MARKER_POINT_TYPE);
        var polyline = createPolyline(markerPrevious,markerNext);



        google.maps.event.addListener(polyline.polyline, 'rightclick', function(mouseEvent){
            document.getElementById('directionsOriginalTransitItem').style.display='block';
            document.getElementById('directionsTransitOriginalItem').style.display='block';
            document.getElementById('directionsTransitItem').style.display='block';
            document.getElementById('directionsOriginItem').style.display='none';
            document.getElementById('directionsRemoveItem').style.display='none';
            document.getElementById('directionsRemoveTransitItem').style.display='none';
            document.getElementById('directionsTransitPointItem').style.display='none';
            document.getElementById('directionsOriginalPointItem').style.display='none';
            document.getElementById('directionsCancelItem').style.display='none';
            contextMenu.show(mouseEvent.latLng,polyline);
        });

        google.maps.event.addListener(polyline.polyline, 'mousemove', function(mouseEvent){
            moveMarkerPolyLine(mouseEvent.latLng,polyline);
        });

        google.maps.event.addListener(polyline.polyline, 'mouseout', function(mouseEvent){
            outPolyLine(mouseEvent.latLng,polyline);
        });

        if(marker_last!=null){
            marker_last.setMarkerType(MarkerObject.MARKER_POINT_TYPE);
        }
        marker_new.setMarkerType(MarkerObject.MARKER_FINISH_TYPE);
        marker_last = marker_new;

    }else{
        marker_first = marker_new;
        marker_first.setMarkerType(MarkerObject.MARKER_START_TYPE);
    }


}

function moveMarkerPolyLine(latLng,polyline){

    if (current_polyline==polyline){
        if(move_marker_polyline==null){
            move_marker_polyline = createTemporaryMarker(latLng,"Test",polyline);
        }
        move_marker_polyline.getMarker().setPosition(latLng);
    }else{
        current_polyline = polyline;
        if(move_marker_polyline!=null) {
            move_marker_polyline.getMarker().setMap(null);
            if(move_marker_polyline.getStartPolyLine()!=null) {
                move_marker_polyline.getStartPolyLine().polyline.setMap(null);
            }
            if(move_marker_polyline.getEndPolyLine()!=null) {
                move_marker_polyline.getEndPolyLine().polyline.setMap(null);
            }
            move_marker_polyline = null;
        }
    }

}

function createTemporaryMarker(markerLatlng,titleMarker,polyline) {

    var marker = new google.maps.Marker({
        position: markerLatlng,
        map: map,
        draggable:true,
        title:titleMarker
    });

    var markerImage = new google.maps.MarkerImage('',
        new google.maps.Size(16, 16),
        new google.maps.Point(0, 0),
        new google.maps.Point(8, 8));
    marker.setIcon(markerImage);

    var marker_object = new MarkerTemporaryObject(marker);

    google.maps.event.addListener(marker, 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginalTransitItem').style.display='none';
        document.getElementById('directionsTransitOriginalItem').style.display='none';
        document.getElementById('directionsTransitItem').style.display='block';
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsRemoveItem').style.display='none';
        document.getElementById('directionsRemoveTransitItem').style.display='none';
        document.getElementById('directionsTransitPointItem').style.display='none';
        document.getElementById('directionsOriginalPointItem').style.display='none';
        document.getElementById('directionsCancelItem').style.display='none';
        contextMenu.show(mouseEvent.latLng,polyline);
    });

    google.maps.event.addListener(marker, 'dragstart', function(event) {

        //marker_object.setMainMarker(createDraggedTransit(event.latLng,polyline));

        if(marker_object.getStartPolyLine()!=null){
            marker_object.getStartPolyLine().polyline.setMap(null);
            marker_object.setStartPolyLine(null);
        }

        if(marker_object.getEndPolyLine()!=null){
            marker_object.getEndPolyLine().polyline.setMap(null);
            marker_object.setEndPolyLine(null);
        }

        createTempPolyline(polyline.marker_start,marker_object);
        createTempPolyline(marker_object,polyline.marker_end);

    });

    google.maps.event.addListener(marker, 'dragend', function(event) {

        /*document.getElementById('directionsOriginalTransitItem').style.display='none';
         document.getElementById('directionsTransitOriginalItem').style.display='none';
         document.getElementById('directionsTransitItem').style.display='none';
         document.getElementById('directionsOriginItem').style.display='none';
         document.getElementById('directionsRemoveItem').style.display='none';
         document.getElementById('directionsRemoveTransitItem').style.display='none';
         document.getElementById('directionsTransitPointItem').style.display='block';
         document.getElementById('directionsOriginalPointItem').style.display='block';
         document.getElementById('directionsCancelItem').style.display='block';


         contextMenu.show(event.latLng,polyline);
         */

        var marker_map = createMarker(event.latLng,"Test");
        closeInfoWindow();
        getInfoWindowRightMarker(marker_map).open(map, marker_map.getMarker());




        var markerPrevious = marker_object.getStartPolyLine().marker_start;

        if(current_polyline!=null){
            current_polyline.polyline.setMap(null);
        }

        var markerLast = marker_object.getEndPolyLine().marker_end;
        var polyline_start = createPolyline(markerPrevious,marker_map);
        var polyline_end = createPolyline(marker_map,markerLast);
        marker_map.setStartPolyLine(polyline_start);
        marker_map.setEndPolyLine(polyline_end);

        google.maps.event.addListener(polyline_start.polyline, 'mousemove', function(mouseEvent){
            moveMarkerPolyLine(mouseEvent.latLng,polyline_start);
        });

        google.maps.event.addListener(polyline_end.polyline, 'mousemove', function(mouseEvent){
            moveMarkerPolyLine(mouseEvent.latLng,polyline_end);
        });

        //getInfoWindow(marker_map).open(map, marker_map.getMarker());

        console.log("Marker move");
    });

    google.maps.event.addListener(marker, 'drag', function(event) {

        if(marker_object.getMarker()!=null){
            marker_object.getMarker().setPosition(event.latLng);

            var latLng = event.latLng;
            var poly_line_start = marker_object.getStartPolyLine();
            if(poly_line_start!=null) {
                var paths = poly_line_start.polyline.getPath().getArray();
                var path = [paths[0], latLng];
                poly_line_start.polyline.setPath(path);
            }

            var poly_line_end = marker_object.getEndPolyLine();

            if(poly_line_end!=null) {
                var paths = poly_line_end.polyline.getPath().getArray();
                var path = [latLng,paths[1]];
                poly_line_end.polyline.setPath(path);
            }

        }

    });

    return marker_object;
}

function createDraggedTransit(latLng,context){
    context.polyline.setMap(null);
    var markerTransit = createMarker(latLng,"Transit");

    addPointToArray(markerTransit,context.marker_end);

    var transit_polyline = createPolyline(context.marker_start,markerTransit);
    var transit_polyline1 = createPolyline(markerTransit,context.marker_end);

    google.maps.event.addListener(transit_polyline.polyline, 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginalTransitItem').style.display='none';
        document.getElementById('directionsTransitOriginalItem').style.display='none';
        document.getElementById('directionsTransitItem').style.display='block';
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsRemoveItem').style.display='none';
        document.getElementById('directionsRemoveTransitItem').style.display='none';
        document.getElementById('directionsTransitPointItem').style.display='none';
        document.getElementById('directionsOriginalPointItem').style.display='none';
        document.getElementById('directionsCancelItem').style.display='none';
        contextMenu.show(mouseEvent.latLng,transit_polyline);
    });

    google.maps.event.addListener(transit_polyline.polyline, 'mousemove', function(mouseEvent){
        moveMarkerPolyLine(mouseEvent.latLng,transit_polyline);
    });

    google.maps.event.addListener(transit_polyline1.polyline, 'rightclick', function(mouseEvent){
        document.getElementById('directionsOriginalTransitItem').style.display='none';
        document.getElementById('directionsTransitOriginalItem').style.display='none';
        document.getElementById('directionsTransitItem').style.display='block';
        document.getElementById('directionsOriginItem').style.display='none';
        document.getElementById('directionsRemoveItem').style.display='none';
        document.getElementById('directionsRemoveTransitItem').style.display='none';
        document.getElementById('directionsTransitPointItem').style.display='none';
        document.getElementById('directionsOriginalPointItem').style.display='none';
        document.getElementById('directionsCancelItem').style.display='none';
        contextMenu.show(mouseEvent.latLng,transit_polyline1);
    });

    google.maps.event.addListener(transit_polyline1.polyline, 'mousemove', function(mouseEvent){
        moveMarkerPolyLine(mouseEvent.latLng,transit_polyline1);
    });

    return markerTransit;
}

function outPolyLine(latLng,polyline){
    /*if (!is_dragged) {
     if (move_marker_polyline != null) {
     move_marker_polyline.setMap(null);
     move_marker_polyline = null;
     }
     }
     */

}

function createPolyline(markerPrevious,markerNext) {
    var markerType = markerNext.getMarkerType();
    // alert(markerPrevious.getMarkerType());
    var lineColors = [
        "#696969",
        "#0000ff"
    ];
    var lineColor;

    if ( markerType == MarkerObject.MARKER_WATER_TYPE ) {
        lineColor = lineColors[1];
        // alert('water');
    } else {
        lineColor = lineColors[0];
    }




    var lineSymbol = {
        path: 'M 0,-1 0,1',
        strokeOpacity: 1,
        scale: 4,
        strokeColor: lineColor,
    };


    var polyline_item = {};

    var polilyne = new google.maps.Polyline({
        path: [markerPrevious.getMarker().position, markerNext.getMarker().position],
        strokeOpacity: 0,
        strokeWeight: 5,
        scale: 4,
        icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '20px'
        }],
        map: map
    });

    polyline_item.type_item = "PolyLineTransit";

    polyline_item.polyline = polilyne;
    polyline_item.marker_start = markerPrevious;
    polyline_item.marker_end = markerNext;

    markerPrevious.setEndPolyLine(polyline_item);
    markerNext.setStartPolyLine(polyline_item);



    return polyline_item;
}


function createTempPolyline(markerPrevious,markerNext) {



    var polyline_item = {};

    var polilyne = new google.maps.Polyline({
        path: [markerPrevious.getMarker().position,markerNext.getMarker().position],
        geodesic: true,
        strokeColor: '#000000',
        strokeOpacity: 0.3,
        strokeWeight: 2,
        map: map
    });

    polyline_item.type_item = "PolyLineTransit";

    polyline_item.polyline=polilyne;
    polyline_item.marker_start = markerPrevious;
    polyline_item.marker_end = markerNext;

    markerPrevious.setEndPolyLine(polyline_item);
    markerNext.setStartPolyLine(polyline_item);



    return polyline_item;
}




function updateMarker(current_marker){



    alert(current_marker.toJsonString());
}

function updateMarkerInfo(target_marker){
    //
    // if(target_marker.getNearestCityOrWater()=='undefined'){
    //     getInfoWindow(target_marker).open(map, target_marker.getMarker());
    // }
    //
    // if(this.getTypeOfSurface()!=1) {
    //     target_marker.getMarker().setIcon(target_marker.getMarkerIcon(target_marker.getMarkerType()));
    // }else{
    //
    //     if ((this.getMarkerType()==MarkerObject.MARKER_POINT_TYPE)||(this.getMarkerType()==MarkerObject.MARKER_TRANSIT_TYPE))
    //     {
    //         target_marker.getMarker().setIcon(target_marker.getMarkerIcon(MarkerObject.MARKER_WATER_TYPE));
    //     }else{
    //         target_marker.getMarker().setIcon(target_marker.getMarkerIcon(target_marker.getMarkerType()));
    //     }
    //
    // }

    /*
     Обновление маркера
     */
    // console.log(target_marker);

}

function createMarker(markerLatlng,titleMarker) {

    var marker = new google.maps.Marker({
        position: markerLatlng,
        map: map,
        draggable:true,
        title:titleMarker
    });

    var marker_object = new MarkerObject(marker,map,updateMarkerInfo);

    marker.addListener('click', function() {

        getInfoWindow(marker_object).open(map, marker);
    });

    marker.addListener('rightclick', function() {
        closeInfoWindow();
        getInfoWindowRightMarker(marker_object).open(map, marker);
    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        var latLng = event.latLng;
        marker_object.updateLatLngInfo(latLng);
        console.log(latLng);
    });

    google.maps.event.addListener(marker, 'drag', function(event) {
        var latLng = event.latLng;
        var poly_line_start = marker_object.getStartPolyLine();

        if(poly_line_start!=null) {
            var paths = poly_line_start.polyline.getPath().getArray();
            var path = [paths[0], latLng];
            poly_line_start.polyline.setPath(path);
        }

        var poly_line_end = marker_object.getEndPolyLine();

        if(poly_line_end!=null) {
            var paths = poly_line_end.polyline.getPath().getArray();
            var path = [latLng,paths[1]];
            poly_line_end.polyline.setPath(path);
        }

    } );


    return marker_object;
}



function originalToTransitMarker(){

    closeInfoWindow();
    clearTemporaryMarker();
    current_marker_object.setMarkerType(MarkerObject.MARKER_TRANSIT_TYPE);
}

function removeMarker(){

    closeInfoWindow();
    clearTemporaryMarker();
    removePointFromArray(current_marker_object);

    current_marker_object.getMarker().setMap(null);

    if(current_marker_object==marker_last){
        var polyLine_start = current_marker_object.getStartPolyLine();

        polyLine_start.polyline.setMap(null);

        var start_marker = polyLine_start.marker_start;
        if(start_marker==marker_first){
            marker_last=null;
            start_marker.setEndPolyLine(null);

        }else{
            start_marker.setMarkerType(MarkerObject.MARKER_FINISH_TYPE);
            marker_last = start_marker;
            start_marker.setEndPolyLine(null);
        }

    }else{
        if(current_marker_object==marker_first){
            var polyLine_end = current_marker_object.getEndPolyLine();
            if(polyLine_end==null){
                marker_first=null;
            }else{

                var end_marker = polyLine_end.marker_end;
                if(end_marker==marker_last){
                    marker_first = end_marker;
                    end_marker.setEndPolyLine(null);
                    marker_last=null;
                }else{
                    marker_first = end_marker;
                    end_marker.setStartPolyLine(null);
                    end_marker.setMarkerType(MarkerObject.MARKER_START_TYPE);
                }
                polyLine_end.polyline.setMap(null);

            }
        }else{
            var polyLine_start = current_marker_object.getStartPolyLine();
            var polyLine_end = current_marker_object.getEndPolyLine();

            var start_marker = polyLine_start.marker_start;
            var end_marker = polyLine_end.marker_end;

            if((start_marker.getMarkerType() != MarkerObject.MARKER_TRANSIT_TYPE)&&(end_marker.getMarkerType() != MarkerObject.MARKER_TRANSIT_TYPE)){
                var polyline_new = createPolyline(start_marker,end_marker);
                google.maps.event.addListener(polyline_new.polyline, 'rightclick', function(mouseEvent){
                    document.getElementById('directionsOriginalTransitItem').style.display='none';
                    document.getElementById('directionsTransitOriginalItem').style.display='none';
                    document.getElementById('directionsTransitItem').style.display='block';
                    document.getElementById('directionsOriginItem').style.display='none';
                    document.getElementById('directionsRemoveItem').style.display='none';
                    document.getElementById('directionsRemoveTransitItem').style.display='none';
                    document.getElementById('directionsTransitPointItem').style.display='none';
                    document.getElementById('directionsOriginalPointItem').style.display='none';
                    document.getElementById('directionsCancelItem').style.display='none';
                    contextMenu.show(mouseEvent.latLng,polyline_new);
                });
                google.maps.event.addListener(polyline_new.polyline, 'mousemove', function(mouseEvent){
                    moveMarkerPolyLine(mouseEvent.latLng,polyline_new);
                });


            }else{
                var polyline_transit_new = createPolyline(start_marker,end_marker);
                google.maps.event.addListener(polyline_transit_new.polyline, 'rightclick', function(mouseEvent){
                    document.getElementById('directionsOriginalTransitItem').style.display='none';
                    document.getElementById('directionsTransitOriginalItem').style.display='none';
                    document.getElementById('directionsTransitItem').style.display='block';
                    document.getElementById('directionsOriginItem').style.display='none';
                    document.getElementById('directionsRemoveItem').style.display='none';
                    document.getElementById('directionsRemoveTransitItem').style.display='none';
                    document.getElementById('directionsTransitPointItem').style.display='none';
                    document.getElementById('directionsOriginalPointItem').style.display='none';
                    document.getElementById('directionsCancelItem').style.display='none';
                    contextMenu.show(mouseEvent.latLng,polyline_transit_new);
                });
                google.maps.event.addListener(polyline_transit_new.polyline, 'mousemove', function(mouseEvent){
                    moveMarkerPolyLine(mouseEvent.latLng,polyline_transit_new);
                });
            }

            polyLine_start.polyline.setMap(null);
            polyLine_end.polyline.setMap(null);

        }
    }
    current_marker_object = null;
}

function transitToOriginalMarker(){

    closeInfoWindow();
    clearTemporaryMarker();
    current_marker_object.setMarkerType(MarkerObject.MARKER_POINT_TYPE);
}

function getInfoWindowRightGoogle(){

}

function onChangeText(text_value){

    if (checkInputText(text_value)) {

        closeInfoWindow();
        clearTemporaryMarker();
        if (current_marker_object.getTypeOfSurface() == 1) {
            current_marker_object.setWaterName(text_value);
        }
        current_marker_object.setNearestCity(text_value);
    }
}

function checkInputText(text_value){

    var re = /^\d{3,}$/;
    return re.test(text_value);
}


function getInfoWindowRightMarker(marker_object){

    current_marker_object = marker_object;
    var content = '<div class="container-infoDivRight">';

    if(marker_object.getMarkerType()==MarkerObject.MARKER_POINT_TYPE){
        content += '<div class="change-buttonDivRight" onclick="removeMarker()">'+
            '<div class="change-iconLeft">'+
            '<img src="icons/buttons/delete_icon_button.png" height="32" width="24"/>'+
            '</div>'+
            '<div class="change-text">'+
            'Delete point'+
            '</div>'+
            '</div>';

        content += '<div class="change-buttonDivRight" onclick="originalToTransitMarker()">'+
            '<div class="change-iconLeft">'+
            '<img src="icons/markers/transit_marker.png" height="32" width="24"/>'+
            '</div>'+
            '<div class="change-text">'+
            'Select like<br/>transit point'+
            '</div>'+
            '</div>';

    }else
    if(marker_object.getMarkerType()==MarkerObject.MARKER_TRANSIT_TYPE){
        content += '<div class="change-buttonDivRight" onclick="removeMarker()">'+
            '<div class="change-iconLeft">'+
            '<img src="icons/buttons/delete_icon_button.png" height="32" width="24"/>'+
            '</div>'+
            '<div class="change-text">'+
            'Delete point'+
            '</div>'+
            '</div>';

        content += '<div class="change-buttonDivRight" onclick="transitToOriginalMarker()">'+
            '<div class="change-iconLeft">'+
            '<img src="icons/markers/point_marker.png" height="32" width="24"/>'+
            '</div>'+
            '<div class="change-text">'+
            'Select like<br/>point'+
            '</div>'+
            '</div>';
    }else{
        content += '<div class="change-buttonDivRight" onclick="removeMarker()">'+
            '<div class="change-iconLeft">'+
            '<img src="icons/buttons/delete_icon_button.png" height="32" width="24"/>'+
            '</div>'+
            '<div class="change-text">'+
            'Delete point'+
            '</div>'+
            '</div>';
    }

    content +='</div>';

    infowindow.setContent(content);

    return infowindow;

}

function getInfoTemporaryMarker(){
    var content ='';

    content = '<div class="container-infoDiv">' +
        '<div>' +
        '<img class="img-iconLeft" src="'+country_flags_url+marker_object.getCountryCode() +'.png" height="17" width="17">' +
        '<div class="div-subTitle">' + marker_object.getNearestCityOrWater() + '</div>' +
        '</div>' +
        '<div class="div-center">' +
        '<img class="img-iconLeft" src="icons/images/icon_lat_lng.png" height="12" width="12">' +
        '<div>' + (Math.floor(marker_object.getMarker().getPosition().lat()*100000+0.5)/100000) + ',' + (Math.floor(marker_object.getMarker().getPosition().lng()*100000+0.5)/100000) + '</div>' +
        '</div>';

    content += '<div class="change-buttonDiv" onclick="originalToTransitMarker()">' +
        '<div class="change-iconLeft">' +
        '<img src="icons/images/change_icon_button.png" height="16" width="16"/>' +
        '</div>' +
        '<div class="change-text">' +
        'Change the type of<br/>points for a transit' +
        '</div>' +
        '<div class="change-iconRight">' +
        '<img src="icons/markers/transit_marker.png" height="32" width="24"/>' +
        '</div>' +
        '</div>' +
        '<div class="container-descriptionDiv">' +
        'Transit - this point, is not<br/>described in the report' +
        '</div>';

    content += '</div>';

    infowindow.setContent(content);

    return infowindow;

}


function getInfoWindow(marker_object){
    current_marker_object = marker_object;
    var content ='';
    if (marker_object.getNearestCity()=='undefined'){

        content = '<div class="container-infoDiv">' +
            '<div class="div-center">' +
            '<img class="img-iconLeft" src="icons/images/icon_lat_lng.png" height="12" width="12">' +
            '<div>' + (Math.floor(marker_object.getMarker().getPosition().lat()*100000+0.5)/100000) + ',' + (Math.floor(marker_object.getMarker().getPosition().lng()*100000+0.5)/100000) + '</div>' +
            '</div>';
        content +='<div class="container-notfoundText">'+
            'We have not found a place<br/>'+
            'on the map, please enter<br/>'+
            'here<br/>'+
            '</div>'+

            '<input class="container-inputText" type="text" onchange="onChangeText(this.value)" pattern="\d{3,}" required>';

        content += '</div>';

    }else {

        content = '<div class="container-infoDiv">' +
            '<div>' +
            '<img class="img-iconLeft" src="'+country_flags_url+marker_object.getCountryCode() +'.png" height="17" width="17">' +
            '<div class="div-subTitle">' + marker_object.getNearestCityOrWater() + '</div>' +
            '</div>' +
            '<div class="div-center">' +
            '<img class="img-iconLeft" src="icons/images/icon_lat_lng.png" height="12" width="12">' +
            '<div>' + (Math.floor(marker_object.getMarker().getPosition().lat()*100000+0.5)/100000) + ',' + (Math.floor(marker_object.getMarker().getPosition().lng()*100000+0.5)/100000) + '</div>' +
            '</div>';

        if (marker_object.getMarkerType() == MarkerObject.MARKER_POINT_TYPE) {
            content += '<div class="change-buttonDiv" onclick="originalToTransitMarker()">' +
                '<div class="change-iconLeft">' +
                '<img src="icons/images/change_icon_button.png" height="16" width="16"/>' +
                '</div>' +
                '<div class="change-text">' +
                'Change the type of<br/>points for a transit' +
                '</div>' +
                '<div class="change-iconRight">' +
                '<img src="icons/markers/transit_marker.png" height="32" width="24"/>' +
                '</div>' +
                '</div>' +
                '<div class="container-descriptionDiv">' +
                'Transit - this point, is not<br/>described in the report' +
                '</div>';
        } else if (marker_object.getMarkerType() == MarkerObject.MARKER_TRANSIT_TYPE) {
            content += '<div class="change-buttonDiv" onclick="transitToOriginalMarker()">' +
                '<div class="change-iconLeft">' +
                '<img src="icons/images/change_icon_button.png" height="16" width="16"/>' +
                '</div>' +
                '<div class="change-text">' +
                'Change the type of<br/>transit points for a points' +
                '</div>' +
                '<div class="change-iconRight">' +
                '<img src="icons/markers/point_marker.png" height="32" width="24"/>' +
                '</div>' +
                '</div>';
        }

        content += '</div>';
    }

    infowindow.setContent(content);

    return infowindow;

}



function callNext(){

    if(marker_list.length==0)
        return;

    var current_index_marker = -1;
    if (current_marker_object){
        for (var i=0; i<marker_list.length; i++) {
            if(marker_list[i]==current_marker_object){
                current_index_marker = i;
                break;
            }
        }
    }

    current_index_marker = current_index_marker+1;

    if(current_index_marker>=marker_list.length){
        current_index_marker = 0;
    }

    closeInfoWindow();

    var target_marker = marker_list[current_index_marker];
    getInfoWindow(target_marker).open(map, target_marker.getMarker());

}

function callPrev(){
    if(marker_list.length==0)
        return;

    var current_index_marker = -1;
    if (current_marker_object){
        for (var i=0; i<marker_list.length; i++) {
            if(marker_list[i]==current_marker_object){
                current_index_marker = i;
                break;
            }
        }
    }

    current_index_marker = current_index_marker-1;

    if(current_index_marker<0){
        current_index_marker = marker_list.length-1;
    }

    closeInfoWindow();

    var target_marker = marker_list[current_index_marker];
    getInfoWindow(target_marker).open(map, target_marker.getMarker());
}
